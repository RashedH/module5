#!/bin/bash
# A simple reminder utility
echo -e "R3m!ndEr - A unix sticky notes service\n\n"
LOGFILE="$HOME/.reminder"
# if there's no argument
if [ $# -eq 0 ] || ([ $# -eq 1 ] && [ $1 == "-i" ]) ; then
    # Prompt the user for input
    echo "Enter notes to remember, end with ^D: "
    # append whatever they write to the logfile.
    cat >> $LOGFILE
elif [ $# -eq 1 ] && [ $1 == "-d" ] ; then
    more $LOGFILE
elif [ $# -eq 2 ] && [ $1 == "-s" ] ; then
    grep -rnw $LOGFILE -e $2 | more
else
    echo -e "Usage:\n./reminder.sh\t\t\t - Enter text to remember\n./reminder.sh -i\t\t - Enter text to remember\n./reminder.sh -d\t\t - Display already saved remindertexts\n./reminder.sh -s \"text\"\t\t - Search through already saved reminder texts\n"
fi