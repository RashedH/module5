#!/bin/bash
echo -e "Cal - A unix calendar service\n\n"
LOGFILE="$HOME/.cal"
isDayName()
{
    # Return 0 if all is well, 1 on error.
    case $(echo $1 | tr '[[:upper:]]' '[[:lower:]]') in
    sun*|mon*|tue*|wed*|thu*|fri*|sat*) retval=0 ;;
    * ) retval=1 ;;
    esac
    return $retval
}
isMonthName()
{
    case $(echo $1 | tr '[[:upper:]]' '[[:lower:]]') in
    jan*|feb*|mar*|apr*|may|jun*) return 0 ;;
    jul*|aug*|sep*|oct*|nov*|dec*) return 0 ;;
    * ) return 1 ;;
    esac
}
➊ normalize()
{
    # Return string with first char uppercase, next two lowercase.
    /bin/echo -n $1 | cut -c1 | tr '[[:lower:]]' '[[:upper:]]'
    echo $1 | cut -c2-3| tr '[[:upper:]]' '[[:lower:]]'
}

# if there's no argument
if [ $# -eq 0 ] || ([ $# -eq 1 ] && [ $1 == "-a" ]) ; then
    # Prompt the user for input
    echo "Enter notes to remember, end with ^D: "
    # append whatever they write to the logfile.
    cat >> $LOGFILE
elif [ $# -eq 1 ] && [ $1 == "-d" ] ; then
    more $LOGFILE
elif [ $# -eq 2 ] && [ $1 == "-s" ] ; then
    grep -rnw $LOGFILE -e $2 | more
else
    echo -e "Usage:\n./cal.sh\t\t\t - Enter text to remember\n./reminder.sh -i\t\t - Enter text to remember\n./reminder.sh -d\t\t - Display already saved remindertexts\n./reminder.sh -s \"text\"\t\t - Search through already saved reminder texts\n"
fi