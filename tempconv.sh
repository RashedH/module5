#!/bin/bash
echo -e "TempConv - A unix temperature conversion service\n\n"
if [ $# -eq 0 ] ; then
    echo "Enter unit of temperature to convert from (either F, K or C):"
    read unit_from
    echo "Enter unit of temperature to convert to (either F, K or C):"
    read unit_to
    echo "Enter value of input temperature:"
    read value
    if ([ $unit_from = "F" ] && [ $unit_to = "C" ]) ; then
        temp=`expr $value - 32`
        temp=`expr $temp \* 5`
        temp=`expr $temp / 9`
        echo "$value Fahrenheit to Celcius = $temp"
    elif ([ $unit_from = "F" ] && [ $unit_to = "K" ]) ; then
        temp=`expr $value - 32`
        temp=`expr $temp \* 5`
        temp=`expr $temp / 9`
        temp=`expr $temp + 273.15`
        echo "$value Fahrenheit to Kelvin = $temp"
    elif ([ $unit_from = "C" ] && [ $unit_to = "F" ]) ; then
        temp=`expr $value \* 9`
        temp=`expr $temp / 5`
        temp=`expr $temp + 32`
        echo "$value Celcius to Fahrenheit = $temp"
    elif ([ $unit_from = "C" ] && [ $unit_to = "K" ]) ; then
        temp=`expr $value + 273.15`
        echo "$value Celcius to Fahrenheit = $temp"
    elif ([ $unit_from = "K" ] && [ $unit_to = "C" ]) ; then
        temp=`expr $value - 273.15`
        echo "$value Kelvin to Celcius = $temp"
    elif ([ $unit_from = "K" ] && [ $unit_to = "F" ]) ; then
        temp=`expr $value - 273.15`
        temp=`expr $temp \* 9`
        temp=`expr $temp / 5`
        temp=`expr $temp + 32`
        echo "$value Kelvin to Fahrenheit = $temp"
    fi
fi